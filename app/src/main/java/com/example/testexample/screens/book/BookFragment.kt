package com.example.testexample.screens.book

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.testexample.adapters.BookAdapter
import com.example.testexample.base.BaseFragment
import com.example.testexample.databinding.FragmentBookBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class BookFragment : BaseFragment<FragmentBookBinding>() {

    private val viewModel by viewModel<BookViewModel>()
    private val bookAdapter = BookAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBookBinding = FragmentBookBinding.inflate(inflater, container, false)

    override fun FragmentBookBinding.onBindView(savedInstanceState: Bundle?) {
        booksList.adapter = bookAdapter
    }

    override fun FragmentBookBinding.subscribeLiveData() {
        viewModel.resultLiveData.observe(viewLifecycleOwner) { books ->
            bookAdapter.submitList(books)
        }
    }

}