package com.example.testexample.screens.book

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.dto.BookDto
import com.example.domain.usecase.GetBooksUseCase
import kotlinx.coroutines.launch

class BookViewModel(
    private val getBooksUseCase: GetBooksUseCase,
) : ViewModel() {

    private val _resultLiveData = MutableLiveData<List<BookDto>>()
    val resultLiveData: LiveData<List<BookDto>> = _resultLiveData

    init {
        viewModelScope.launch {
            _resultLiveData.value = getBooksUseCase.execute(10)
        }
    }
}