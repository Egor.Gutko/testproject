package com.example.testexample.di

import androidx.room.Room
import com.example.data.RetrofitCreator
import com.example.data.converters.BookEntityToBookConverter
import com.example.data.converters.BookToBookDtoConverter
import com.example.data.converters.BookToBookEntityConverter
import com.example.data.repoimpl.FakeApiService
import com.example.data.repoimpl.database.BookDBDataSource
import com.example.data.repoimpl.database.BookDataBase
import com.example.data.repoimpl.net.BookNetDataSource
import com.example.data.repoimpl.FakeApiRepositoryImpl
import com.example.domain.repositories.FakeRepository
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val dataModule = module {

    single { GsonBuilder().serializeNulls().create() }
    val creator = RetrofitCreator()

    single {
        Room.databaseBuilder(androidApplication(), BookDataBase::class.java, "book_data_base")
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<BookDataBase>().bookDao() }

    single { creator.createService(get(), FakeApiService::class.java) as FakeApiService }

    single<BookNetDataSource> { BookNetDataSource(get()) }
    single<BookDBDataSource> { BookDBDataSource(get(), get()) }

    factory { BookToBookDtoConverter() }
    factory { BookEntityToBookConverter() }
    factory { BookToBookEntityConverter() }

    single<FakeRepository> { FakeApiRepositoryImpl(get(), get(), get(), get()) }
}