package com.example.testexample.di

import com.example.domain.usecase.GetBooksUseCase
import org.koin.dsl.module

val domainModule = module {

    factory<GetBooksUseCase> { GetBooksUseCase(get()) }
}