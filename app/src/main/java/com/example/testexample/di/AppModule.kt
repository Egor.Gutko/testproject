package com.example.testexample.di

import com.example.testexample.screens.book.BookViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<BookViewModel> {
        BookViewModel(get())
    }
}