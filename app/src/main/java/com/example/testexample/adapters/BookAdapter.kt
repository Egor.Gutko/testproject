package com.example.testexample.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.domain.dto.BookDto
import com.example.testexample.R
import com.example.testexample.base.BaseViewHolder
import com.example.testexample.databinding.BookItemBinding

class BookAdapter :
    ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(object : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is BookDto && newItem is BookDto -> oldItem.id == newItem.id
            else -> false
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is BookDto && newItem is BookDto -> oldItem == newItem
            else -> false
        }

    }) {
    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is BookDto -> BOOK_ITEM_TYPE
        else -> throw  IllegalArgumentException("BookAdapter can't handle item" + getItem(position))
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        BOOK_ITEM_TYPE -> BookViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw  IllegalArgumentException("MyAdapter can't handle $viewType viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val BOOK_ITEM_TYPE = 999
    }
}

private class BookViewHolder(private val parent: ViewGroup) :
    BaseViewHolder<BookItemBinding, BookDto>(
        BookItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    override fun BookItemBinding.bind(value: BookDto) {
        author.text = value.author
        title.text = value.title
        Glide.with(parent.context).load(value.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(image)
    }
}