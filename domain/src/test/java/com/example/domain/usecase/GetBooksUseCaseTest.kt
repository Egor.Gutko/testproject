package com.example.domain.usecase

import com.example.domain.dto.BookDto
import com.example.domain.repositories.FakeRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock

class GetBooksUseCaseTest {

    private companion object {
        const val ID_TEST = 1
        const val TITLE_TEST = "title"
        const val AUTHOR_TEST = "author"
        const val GENRE_TEST = "genre"
        const val DESCRIPTION_TEST = "description"
        const val ISBN_TEST = "isbn"
        const val IMAGE_TEST = "image"
        const val PUBLISHED_TEST = "published"
        const val PUBLISHER_TEST = "publisher"

        const val COUNT_TEST = 1
        const val LOCALE_TEST = "en_US"
    }

    private val fakeRepository = mock<FakeRepository>()

    @AfterEach
    fun tearDown() {
        Mockito.reset(fakeRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `should return the same list of books as in the repository`() = runTest {

        val testList = listOf(
            BookDto(
                ID_TEST,
                TITLE_TEST,
                AUTHOR_TEST,
                GENRE_TEST,
                DESCRIPTION_TEST,
                ISBN_TEST,
                IMAGE_TEST,
                PUBLISHED_TEST,
                PUBLISHER_TEST,
            )
        )

        Mockito.`when`(fakeRepository.getBooks(COUNT_TEST, LOCALE_TEST)).thenReturn(testList)

        val useCase = GetBooksUseCase(fakeRepository)
        val actualResult = useCase.execute(COUNT_TEST)

        val expectedResult = listOf(
            BookDto(
                ID_TEST,
                TITLE_TEST,
                AUTHOR_TEST,
                GENRE_TEST,
                DESCRIPTION_TEST,
                ISBN_TEST,
                IMAGE_TEST,
                PUBLISHED_TEST,
                PUBLISHER_TEST,
            )
        )

        Assertions.assertEquals(expectedResult, actualResult)
    }
}