package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.dto.BookDto
import com.example.domain.repositories.FakeRepository

class GetBooksUseCase(private val fakeRepository: FakeRepository) : UseCase<Int, List<BookDto>> {
    override suspend fun execute(param: Int?): List<BookDto> {
        return fakeRepository.getBooks(param!!, "en_US")
    }
}