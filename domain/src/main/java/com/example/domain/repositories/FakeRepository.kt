package com.example.domain.repositories

import com.example.domain.dto.BookDto

interface FakeRepository {

    suspend fun getBooks(quantity: Int, locale: String): List<BookDto>
}