package com.example.data.converters

import com.example.data.model.Book
import com.example.domain.dto.BookDto
import retrofit2.Converter

class BookToBookDtoConverter : Converter<Book, BookDto> {
    override fun convert(value: Book): BookDto {
        return BookDto(
            value.id ?: -1,
            value.title ?: "",
            value.author ?: "",
            value.genre ?: "",
            value.description ?: "",
            value.isbn ?: "",
            value.image ?: "",
            value.published ?: "",
            value.publisher ?: ""
        )
    }
}