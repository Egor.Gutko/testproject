package com.example.data.converters

import com.example.data.model.Book
import com.example.data.repoimpl.database.BookEntity
import retrofit2.Converter

class BookToBookEntityConverter : Converter<Book, BookEntity> {
    override fun convert(value: Book): BookEntity {
        return BookEntity(
            id = value.id,
            title = value.title,
            author = value.author,
            genre = value.genre,
            description = value.description,
            isbn = value.isbn,
            image = value.image,
            publisher = value.publisher,
            published = value.published
        )
    }
}