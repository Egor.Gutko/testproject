package com.example.data.converters

import com.example.data.model.Book
import com.example.data.repoimpl.database.BookEntity
import retrofit2.Converter

class BookEntityToBookConverter : Converter<BookEntity, Book> {
    override fun convert(value: BookEntity): Book {
        return Book(
            id = value.id,
            title = value.title,
            author = value.author,
            genre = value.genre,
            description = value.description,
            isbn = value.isbn,
            image = value.image,
            publisher = value.publisher,
            published = value.published
        )
    }
}