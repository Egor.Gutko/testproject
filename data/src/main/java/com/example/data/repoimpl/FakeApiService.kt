package com.example.data.repoimpl

import com.example.data.BOOKS
import com.example.data.BaseResponse
import com.example.data.LOCALE
import com.example.data.QUANTITY
import com.example.data.model.Book
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FakeApiService {

    @GET(BOOKS)
    suspend fun getBooks(
        @Query(QUANTITY) quantity: Int,
        @Query(LOCALE) locale: String
    ): Response<BaseResponse<ArrayList<Book>>>
}