package com.example.data.repoimpl.net

import com.example.data.repoimpl.FakeApiService

class BookNetDataSource(private val fakeApiService: FakeApiService) {

   suspend fun getBooks(quantity: Int, locale: String) = fakeApiService.getBooks(quantity, locale)
}