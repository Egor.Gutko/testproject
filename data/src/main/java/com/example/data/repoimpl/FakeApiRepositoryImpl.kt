package com.example.data.repoimpl

import com.example.data.converters.BookToBookDtoConverter
import com.example.data.converters.BookToBookEntityConverter
import com.example.data.repoimpl.database.BookDBDataSource
import com.example.data.repoimpl.net.BookNetDataSource
import com.example.domain.dto.BookDto
import com.example.domain.repositories.FakeRepository

class FakeApiRepositoryImpl(
    private val bookNetDataSource: BookNetDataSource,
    private val bookDBDataSource: BookDBDataSource,
    private val bookDtoConverter: BookToBookDtoConverter,
    private val bookToBookEntityConverter: BookToBookEntityConverter
) : FakeRepository {

    override suspend fun getBooks(quantity: Int, locale: String): List<BookDto> {
        try {
            val response = bookNetDataSource.getBooks(quantity, locale)
            if (response.isSuccessful) {
                response.body()?.let { baseResponse ->
                    val books = mutableListOf<BookDto>()
                    baseResponse.data?.map { book ->
                        bookDBDataSource.insertBook(bookToBookEntityConverter.convert(book))
                        books.add(bookDtoConverter.convert(book))
                    }
                    return books
                }
            } else {
                val books = mutableListOf<BookDto>()
                bookDBDataSource.getBook().map { book ->
                    books.add(bookDtoConverter.convert(book))
                }
                return books
            }
        } catch(e:Exception) {
            val books = mutableListOf<BookDto>()
            bookDBDataSource.getBook().map { book ->
                books.add(bookDtoConverter.convert(book))
            }
            return books
        }
        return emptyList()
    }
}