package com.example.data.repoimpl.database

import com.example.data.converters.BookEntityToBookConverter
import com.example.data.model.Book

class BookDBDataSource(
    private val bookDao: BookDao, private val bookEntityToBookConverter: BookEntityToBookConverter
) {

    suspend fun getBook(): List<Book> = bookDao.getAllBooks().map { bookEntity ->
        bookEntityToBookConverter.convert(bookEntity)
    }

    suspend fun insertBook(bookEntity: BookEntity) = bookDao.insetAllBooks(bookEntity)
}